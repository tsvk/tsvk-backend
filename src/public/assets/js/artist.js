function checkLiveListeners(live) {
	if (live) {
		$.ajax({
			type: 'GET',
			url: '/api/listeners-live',
			success: function (data) {			
				if (data.listeners) document.getElementById('liveListeners').innerHTML = "Listeners: " + data.listeners;
			},
			error: function (request, status, error) {
				console.log(error);
			}
		});

		setTimeout(() => checkLiveListeners(live), 30000);
	} else {
		setTimeout(() => { 
			$.ajax({
				type: 'GET',
				url: '/api/artist-live',
				success: function (data) {			
					if (data) checkLiveListeners(true)
					else checkLiveListeners(false);
				},
				error: function (request, status, error) {
					console.log(error);
				}
			});
		}, 30000);
	}
}
 
var isPlaying = false;
function play(){
	var button = document.getElementById('play');
	button.setAttribute("onclick", "pause()");
	button.getElementsByTagName('img')[0].src = '/images/pause.svg';
	button.getElementsByTagName('img')[0].title = 'Pause';
	var player = document.getElementById('bg_player');
	player.load(); // reset audio to listen live
	player.play();
	isPlaying = true;
}

function pause(){
	var button = document.getElementById('play');
	button.setAttribute("onclick", "play()");
	button.getElementsByTagName('img')[0].src = '/images/play.svg';
	button.getElementsByTagName('img')[0].title = 'Play';
	var player = document.getElementById('bg_player');
	player.pause();
	isPlaying = false;
}
  
function loadChat(rename) {
	var username = localStorage.getItem('username'), oldUsername;
	if (!username || rename) {
		if (rename && username){
			oldUsername = username;
			username = prompt("Please enter your username.", username);
		} 
		else username = prompt("Please enter your username.", "");
		
		if (username == null || username == oldUsername) return;
		else if (username == '') return loadChat(rename);
		else if (username == 'null' || username == 'undefined') alert('Hacker detected');
		localStorage.setItem('username', username);
	}

	const artist = window.location.pathname.split('/')[1];
	document.getElementById('chat_popup').innerHTML = '<iframe src="https://hack.chat/?tsvk_' + artist + '#' + username +'"></iframe><div class="change_name" onclick="loadChat(true)">Change username</div>';
	document.getElementById('chat').setAttribute("onclick", "hideChat()");
}

function hideChat() {
	document.getElementById('chat_popup').style.display = 'none';
	document.getElementById('chat').setAttribute("onclick", "showChat()");
}

function showChat() {
	document.getElementById('chat_popup').style.display = 'block';
	document.getElementById('chat').setAttribute("onclick", "hideChat()");
}

function checkStream(name) {
	var xhttp = new XMLHttpRequest();
	xhttp.onreadystatechange=function() {
	  if (this.readyState == 4 && this.status == 200) {
		parse_who_and_what(this.responseText, name);
	  }
	};
	if (location.hostname.indexOf('localhost') != -1) xhttp.open("GET", "http://localhost:8000/status-json.xsl", true);
	else xhttp.open("GET", "https://radio.tsvk.live/status-json.xsl", true);
	xhttp.send();
  
	setTimeout(() => { 
        checkStream(name)
    }, 30000);
}

var previousState = false;
function parse_who_and_what(raw_json_data, name) {
	var json_data = JSON.parse(raw_json_data), found = false;
	if (json_data.icestats && json_data.icestats.source) {
	  if ( json_data.icestats.source[0] ){
		//If 1+ streamer
		for (var i in json_data.icestats.source) {
			if (name == json_data.icestats.source[i].listenurl.split('/')[3]) {
				if ( json_data.icestats.source[i].artist && json_data.icestats.source[i].title){
					document.getElementById("whats_playin").innerHTML = json_data.icestats.source[i].artist + ' - ' + json_data.icestats.source[i].title;
				} else if (json_data.icestats.source[i].title){
					document.getElementById("whats_playin").innerHTML = json_data.icestats.source[i].title;
				}  
				document.getElementById("description").innerHTML = json_data.icestats.source[i].server_description;
			}
			found = true;
	    }
	  } else {
	    //If one streamer
		if (name == json_data.icestats.source.listenurl.split('/')[3]) {
			if (json_data.icestats.source.artist && json_data.icestats.source.title){
				document.getElementById("whats_playin").innerHTML = json_data.icestats.source.artist + ' - ' + json_data.icestats.source.title;
			} else if (json_data.icestats.source.title){
				document.getElementById("whats_playin").innerHTML = json_data.icestats.source.title;
			}  
			document.getElementById("description").innerHTML = json_data.icestats.source.server_description;
			found = true;
		}
	  }
	 
	  if (!previousState && found) {
		previousState = true;
		var player = document.getElementById('bg_player');
		player.load();
		if(isPlaying) player.play();
	  }

	} else {
		if (previousState) {
			previousState = false;
			var player = document.getElementById('bg_player');
			player.load();
			if (isPlaying) player.play();
			document.getElementById("description").innerHTML = 'The stream has finished';
			document.getElementById("whats_playin").innerHTML = '';
		}
	}
}