const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const request = require("request");
const Artist = mongoose.model('Artist');

//Live artist
router.get('/artist-live', function (req, res) {

    if (req.headers && req.headers.referer && req.headers.referer.split('/')[3]) {
        const artistUsername = req.headers.referer.split('/')[3];
        
        Artist.findOne({ username: artistUsername }, 'live', function (err, artist) {
            return res.send(artist.live);
        });

    } else {
        return res.sendStatus(404);
    }

});

//Live listeners
router.get('/listeners-live', function (req, res) {
    
    if (req.headers && req.headers.referer && req.headers.referer.split('/')[3]) {
        const artistUsername = req.headers.referer.split('/')[3];
        var user = req.cookies.token;

        Artist.findOne({ username: artistUsername }, 'liveListeners live', function (err, artist) {
            if (artist == null) return res.sendStatus(404);
            if (!artist.live) return res.sendStatus(200);
            //Calculate active user for last minute
            var listeners = 0, currentTime = new Date();
            for (var i in artist.liveListeners) {
                const lastUpdateTime = new Date(artist.liveListeners[i].lastUpdate);
                const diffTime = Math.abs(currentTime - lastUpdateTime); //Difference btwn now and last updateTime in ms
                if (diffTime <= 60000) listeners++;
                else if (artist.liveListeners[i].user == user) listeners++;
            }

            if (!user) {
                user = Math.random().toString(36).substr(2, 7); //Generates unique user Id
                res.cookie('token', user, { maxAge: 60 * 24 * 60 * 60 * 1000, session: false }); //Sends cookie
                listeners++;
                Artist.findOneAndUpdate({ "_id": artist._id }, { $push: {"liveListeners": {"user": user}} }, function (){});
            } else if (!artist.liveListeners.some(e => e.user === user)) {
                listeners++;
                Artist.findOneAndUpdate({ "_id": artist._id }, { $push: {"liveListeners": {"user": user}} }, function (){});
            } else {                    
                const userIndex = artist.liveListeners.findIndex( e => e['user'] === user );     
                Artist.findOneAndUpdate({ "_id": artist._id }, { ["liveListeners." + userIndex + ".lastUpdate"]: new Date() }, function (){}); 
            }
            
            return res.send({'listeners': listeners});            
        });
    } else {
        return res.sendStatus(404);
    }
});

checkLiveArtists();
function checkLiveArtists () {
    
    let ICECAST_STATUS_URL = process.env.ICECAST_STATUS_URL || 'https://radio.tsvk.live/status-json.xsl'
    request({uri: ICECAST_STATUS_URL}, 
        function(error, response, body) {
            var status;
            try {
                status = JSON.parse(body);
            } catch (e) {
                return console.log("Parsing JSON error");
            }
            var activeArtists = [];
            if (status.icestats && status.icestats.source && status.icestats.source.listenurl) { //One streamer
                activeArtists.push(status.icestats.source.listenurl.split('/')[3]);
            } else if (status.icestats && status.icestats.source && status.icestats.source[0]) { //One + streamers
                for (var i in status.icestats.source) {
                    activeArtists.push(status.icestats.source[i].listenurl.split('/')[3]);
                }
            }
            //Remove live badges
            Artist.updateMany({
                    $or: [
                        { streamUrl: '' }, 
                        { streamUrl: {$exists: false} }
                    ]
                }, {live: false}, function() {
                //Set live artists
                if (activeArtists != []) {
                    Artist.updateMany(
                        { username: { $in: activeArtists } },
                        { $set: { live: true } },
                        { multi: true }
                    )
                    .then(result => {})
                    .catch(err => {
                     console.log(err);
                    });
                }
            });
        }
    );    

    setTimeout(() => { 
        checkLiveArtists();
    }, 30000);

}

module.exports = router;