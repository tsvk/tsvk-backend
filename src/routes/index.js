﻿'use strict';
var express = require('express');
var router = express.Router();
var fs = require('fs');
const mongoose = require('mongoose');
require('dotenv').config();

//DataBase connection 
mongoose.connect(process.env.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true }, function (err, db) {
    if (err) {
        console.log('Unable to connect to the server. Please start the server\n'+ err);
    } 
});

//Model connection
fs.readdirSync(__dirname + '/model').forEach(function (filename) {
    if (~filename.indexOf('.js')) require(__dirname + '/model/' + filename)
});

var Artist = mongoose.model('Artist');

router.use('/api', require('./api'));

router.get('/', (req, res) => {
    Artist.find({ active: true }, function (err, artists) {
        return res.render('index', { data: artists });        
    });
});

router.get('/:username', (req, res) => {
    Artist.findOne({ username: req.params.username }, function (err, artist) {
        if (!artist) return res.render('404');
        if (req.headers.host.indexOf('localhost') != -1) artist.host = "http://localhost:8000";
        else artist.host = 'https://radio.tsvk.live'
        return res.render('artist', { data: artist });        
    });
});

module.exports = router;
